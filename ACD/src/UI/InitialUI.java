package UI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import Preprocess.AbbreviationExpander;

/**
 * Initialize the UI, main program.
 */
public class InitialUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private static FACTS_Model model;
	private static JFrame frame;
		
	public static void main(String[] args) throws Exception {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("Running Expander");
		
		int response = JOptionPane.showConfirmDialog(frame, "Would you like to run the Abbreviation Expander?");
		
		if(response == JOptionPane.YES_OPTION) {
			JTextField text = new JTextField();
			text.setText("Please Wait While The Expander is Running");
			text.setEditable(false);
			
			frame.add(text);
			frame.pack();
			frame.setVisible(true);
			
			AbbreviationExpander.main(null);
			frame.setVisible(false);
			
		}		
		
		model = new FACTS_Model();
		model.runModelGUI();	
		
		JOptionPane.showMessageDialog(frame, "The Tool has finished running, please check the save directory.");
		frame.dispose();
			
		System.out.println("[FINISHED]");
	}		
	
}
