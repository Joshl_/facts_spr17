package Model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="acd")
@XmlAccessorType(XmlAccessType.FIELD)
public class Issue {

	@XmlElement(name="id")
	String issueID;
	
	@XmlElement(name="author")
	String issueReporter;
	
	@XmlElement(name="type")
	String issueType;
	
	@XmlElement(name="description")
	String issueDescription;
	
	@XmlElement(name="timestamp")
	String issueTimeCreated;
	
	@XmlElement(name="timestampUpdate")
	String issueTimeUpdated;
	
	@XmlElement(name="path")
	String issueLink;

	public String getIssueID() {
		return issueID;
	}

	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}

	public String getIssueReporter() {
		return issueReporter;
	}

	public void setIssueReporter(String issueReporter) {
		this.issueReporter = issueReporter;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getIssueDescription() {
		return issueDescription;
	}

	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}

	public String getIssueTimeCreated() {
		return issueTimeCreated;
	}

	public void setIssueTimeCreated(String issueTimeCreated) {
		this.issueTimeCreated = issueTimeCreated;
	}

	public String getIssueTimeUpdated() {
		return issueTimeUpdated;
	}

	public void setIssueTimeUpdated(String issueTimeUpdated) {
		this.issueTimeUpdated = issueTimeUpdated;
	}

	public String getIssueLink() {
		return issueLink;
	}

	public void setIssueLink(String issueLink) {
		this.issueLink = issueLink;
	}
}
