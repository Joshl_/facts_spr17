package Preprocess;

import java.util.List;

import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;

public class Lemmatizer {

	/**
	 * This method use stanford nlp API to lemmatize the text. It basically
	 * replaces a word with it's correct dictionary root
	 * 
	 * @param content unstructured text from source code files
	 * @return lemmatized text
	 */
	public String lemmas(String content) {

		if(content.isEmpty() || content.equals(" "))
			return content;
		
		String lemmatizedContent = "";

		// Create a document
		Document doc = new Document(content);
		for (Sentence sent : doc.sentences()) {

			List<String> lemmas = sent.lemmas();
			for (String lemma : lemmas) 
				lemmatizedContent = lemmatizedContent + " " + lemma;
		}
		return lemmatizedContent;
	}
}
