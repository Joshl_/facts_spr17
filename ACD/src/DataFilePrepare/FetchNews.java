package DataFilePrepare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringEscapeUtils;

import Preprocess.Preprocessor;

/**
 * Fetch news for the versions and store in XML files.
 * The file should contain all news, separated by lines. 
 * 
 * @author renanpeixotox
 */
public class FetchNews {
	private String workingDirectory, newsFilepath;
	private Preprocessor preprocessor;
	
	public FetchNews(String workingDirectory, String newsFilepath) {
		
		preprocessor = new Preprocessor();
		
		if(!workingDirectory.endsWith("/")) workingDirectory += "/";
		this.workingDirectory = workingDirectory;			
		this.newsFilepath = newsFilepath;
	}
	
	/**
	 * Get data from the NEWS file and convert everything to ACD format
	 * @throws IOException 
	 */
	public void getNews() throws IOException {
		System.out.println("Loading and processing news...\n");
		
		new File(workingDirectory + "news/preprocessed/").mkdirs();
	
		String line;
		int index = 0;
		File file = new File(newsFilepath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		while((line = reader.readLine()) != null) {
			line = line.trim();				
			
			String news_id = "NEWS" + index++;
			FileWriter fwOutputFile = new FileWriter(workingDirectory + "news/" + news_id + ".xml");
			PrintWriter outputFile = new PrintWriter(new BufferedWriter(fwOutputFile));	
			outputFile.println("<acd>");	
			outputFile.println("\t<id>" + news_id + "</id>");
			outputFile.println(
					"\t<description>" 
					+ "\n\t\t" + StringEscapeUtils.escapeXml11(line).replaceAll("\\n", "\n\t\t").trim() 
					+ "\n\t</description>");
			outputFile.write("</acd>");
			outputFile.close();		
			
			FileWriter fwPreprocessedFile = new FileWriter(workingDirectory + "news/preprocessed/" + news_id + ".xml");
			PrintWriter preprocessedFile = new PrintWriter(new BufferedWriter(fwPreprocessedFile));	
			preprocessedFile.println("<acd>");	
			preprocessedFile.println("<id>" + news_id + "</id>");
			preprocessedFile.println("<description>" + StringEscapeUtils.escapeXml11(preprocessor.fullPreprocess(line)) + "</description>");
			preprocessedFile.write("</acd>");
			preprocessedFile.close();		
		}
		reader.close();
		System.out.println("Loading and processing news... [done]\n");
	}	
}
