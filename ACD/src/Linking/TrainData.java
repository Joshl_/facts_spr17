package Linking;

import java.io.File;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffLoader;
import weka.core.converters.Loader;

/**
 * The TrainData class is responsible for loading the training set and building
 * a classifier with the training data. It then serializes the classifier or the
 * prediction model to disk.
 * 
 * @author renanpeixotox
 */
public class TrainData {

	/**
	 * Train the classifier using a predefined training data set. 
	 * Create and save a prediction model (classifier) to disk, which will be used by the prediction class. 
	 * 
	 * @throws Exception
	 */
	public static void trainData(String trainingSetFilepath, String classifierFilepath, 
			boolean f01_ON, boolean f02_ON, boolean f03_ON, boolean f04_ON, boolean f05_f07_ON, 
			boolean f08_f10_ON, boolean f11_ON, boolean f12_ON, boolean f13_ON) throws Exception {
		
		// load the training data
		ArffLoader trainLoader = new ArffLoader();
		trainLoader.setSource(new File(trainingSetFilepath));
		trainLoader.setRetrieval(Loader.BATCH);
		Instances trainDataSet = trainLoader.getDataSet();

		// set attribute to be classified: @attribute link {0,1}
		trainDataSet.setClass(trainDataSet.attribute(0));

		// remove all string attributes or columns for Random Forest
		trainDataSet.deleteStringAttributes();
		
		//remove cosine attributes
		if(f01_ON == false) trainDataSet.deleteAttributeAt(trainDataSet.attribute("nCommonTerms").index());
		if(f02_ON == false) trainDataSet.deleteAttributeAt(trainDataSet.attribute("cosine").index());
		if(f03_ON == false) trainDataSet.deleteAttributeAt(trainDataSet.attribute("maxCosine").index());
		if(f04_ON == false) trainDataSet.deleteAttributeAt(trainDataSet.attribute("avgCosine").index());
		

		//remove weighted attributes
		if(f05_f07_ON == false) {
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("cosineFeatures").index());
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("cosineArchConcepts").index());
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("cosineOpConcepts").index());
		}
		
		//remove n-grams attributes
		if(f08_f10_ON == false) {
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("ngramsFeatures").index());
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("ngramsArchConcepts").index());
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("ngramsOpConcepts").index());
		}
		
		//remove date attributes
		if(f11_ON == false && trainDataSet.attribute("cDate_between_iDates") != null) 
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("cDate_between_iDates").index());
		
		if(f12_ON == false && trainDataSet.attribute("comDate_minus_iRepDate") != null) 
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("comDate_minus_iRepDate").index());
		
		if(f13_ON == false && trainDataSet.attribute("iUpDate_minus_comDate") != null) 
			trainDataSet.deleteAttributeAt(trainDataSet.attribute("iUpDate_minus_comDate").index());

		// create and configure the Random Forest classifier
		RandomForest classifier = new RandomForest();
		
		// train the classifier
		classifier.buildClassifier(trainDataSet);

		// save prediction model to disk
		SerializationHelper.write(classifierFilepath, classifier);
		System.out.println("Classifier was successfully built and saved to '" + classifierFilepath + "'.\n");
	}
}
