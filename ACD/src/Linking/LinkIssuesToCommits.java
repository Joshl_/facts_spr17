package Linking;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import Model.Commit;
import Model.Issue;
import Preprocess.Preprocessor;

/**
 * This class is responsible for processing commits and JIRA issues in order to
 * extract commit messages (or reasons for changes) and bug reports, pair each commit
 * with each JIRA issue and calculate text similarities between them.
 * 
 * @author renanpeixotox
 */
public class LinkIssuesToCommits {
	
	private final int N_THREADS = 4;
	
	private Preprocessor preprocessor;
	
	private Features featuresProcessor;
	
	private boolean f01_ON; 
	private boolean f02_ON; 
	private boolean f03_ON; 
	private boolean f04_ON; 
	private boolean f11_ON; 
	private boolean f12_ON; 
	private boolean f13_ON; 
	private boolean f05_f07_ON;
	private boolean f08_f10_ON;
	
	private String workingDirectory;
	
	private String trainingSetFilepath;
	private String classifierFilepath;
	private String predictionFilepath;
	private String resultSetFilepath;
	private String similaritiesFilepath;
	
	private PrintWriter pw_similaritiesFile;
	
	/**
	 * Initialize important elements to be used: create files to
	 * store outputs, define initial set-ups and attributes, and start utility
	 * classes or libraries.
	 * @throws FileNotFoundException 
	 * @throws Exception 
	 */
	public LinkIssuesToCommits(String workingDirectory, String keyTermsDirectory, String trainingSetFilepath,
			boolean f01_ON, boolean f02_ON, boolean f03_ON, boolean f04_ON, boolean f05_f07_ON, 
			boolean f08_f10_ON, boolean f11_ON, boolean f12_ON, boolean f13_ON) throws FileNotFoundException {
		
		if(!workingDirectory.endsWith("/")) workingDirectory += "/";
		if(!keyTermsDirectory.endsWith("/")) keyTermsDirectory += "/";
		this.workingDirectory = workingDirectory;

		new File(workingDirectory + "prediction/issue-commit/").mkdirs();	
		new File(workingDirectory + "visualize/").mkdirs();	
		
		preprocessor = new Preprocessor();
		featuresProcessor = new Features(preprocessor, keyTermsDirectory);
		
		this.f01_ON = f01_ON;
		this.f02_ON = f02_ON;
		this.f03_ON = f03_ON;
		this.f04_ON = f04_ON;
		this.f11_ON = f11_ON;
		this.f12_ON = f12_ON;
		this.f13_ON = f13_ON;
		this.f05_f07_ON = f05_f07_ON;
		this.f08_f10_ON = f08_f10_ON;
		
		resultSetFilepath = workingDirectory + "visualize/resultSet_Issue-Commit.csv";
		similaritiesFilepath = workingDirectory + "prediction/issue-commit/similarities_Issue-Commit.arff";
		predictionFilepath = workingDirectory + "prediction/issue-commit/prediction_Issue-Commit.arff";
		classifierFilepath = workingDirectory + "prediction/issue-commit/classifier_Issue-Commit.model";	
		this.trainingSetFilepath = trainingSetFilepath;
				
		// define header section of the relation file
		String similaritiesFileHeader = 
				"@relation commit_issues\n\n" 
				+ "@attribute link {0,1}\n"
				+ "@attribute issueID string\n"
				+ "@attribute commitID string\n";
		
		if(f01_ON) similaritiesFileHeader += "@attribute nCommonTerms numeric\n"; 						// F1 = number of terms in common
		if(f02_ON) similaritiesFileHeader += "@attribute cosine numeric\n";								// F2 = cosine similarity
		if(f03_ON) similaritiesFileHeader += "@attribute maxCosine numeric\n"; 							// F3 = max of cosine
		if(f04_ON) similaritiesFileHeader += "@attribute avgCosine numeric\n";							// F4 = average of cosine
				
		if(f05_f07_ON)
			similaritiesFileHeader +=
				"@attribute cosineFeatures numeric\n"													// F5 = weighted cosine features
				+ "@attribute cosineArchConcepts numeric\n"												// F6 = weighted cosine archConcepts
				+ "@attribute cosineOpConcepts numeric\n";												// F7 = weighted cosine operationConcepts
		
		if(f08_f10_ON)
			similaritiesFileHeader +=		
				"@attribute ngramsFeatures numeric\n"													// F8 = n-grams features
				+ "@attribute ngramsArchConcepts numeric\n"												// F9 = n-grams archConcepts
				+ "@attribute ngramsOpConcepts numeric\n";												// F10 = n-grams operationConcepts
	
		if(f11_ON) similaritiesFileHeader += "@attribute cDate_between_iDates {true,false}\n";			// F11 = (issueReportDate < commitDate < issueUpdateDate) ? true : false
		if(f12_ON) similaritiesFileHeader += "@attribute comDate_minus_iRepDate numeric\n";				// F12 = (commitDate - issueReportDate)
		if(f13_ON) similaritiesFileHeader += "@attribute iUpDate_minus_comDate numeric\n";				// F13 = (issueUpdateDate - commitDate)

		similaritiesFileHeader += "\n@data";

		// create the (relation) file to store the similarity metrics that will be calculated
		File similaritiesFile = new File(similaritiesFilepath);
		pw_similaritiesFile = new PrintWriter(similaritiesFile);
		pw_similaritiesFile.println(similaritiesFileHeader);
	}
	
	/**
	 * Pair each issue with each commit and calculate similarities 
	 * @throws Exception 
	 */
	public void pairIssuesWithCommits() throws Exception {
		
		/**
		 * Defines a runnable object for pairing issues with commits in a specified range (start, end)
		 * @author renanpeixotox
		 */
		class PairIssuesWithCommitsInRange implements Runnable {
			int start, end;
			File[] commitFiles;
	        PairIssuesWithCommitsInRange(int start, int end, File[] commitFiles) { 
	        	this.end = end;
	        	this.start = start;
	        	this.commitFiles = commitFiles; 
	        }
	        public void run() {
	        	try {
					doThis(start, end, commitFiles);
				} catch (FileNotFoundException | JAXBException | ParseException e) {
					e.printStackTrace();
				}
	        }
	    }
		
		System.out.println("Processing logs and generating issue-commit links...");
		
		Thread[] t = new Thread[N_THREADS];
		File[] commitFiles = new File(workingDirectory + "commits/preprocessed/").listFiles();
		
		//splits the number of files to be run in different N_THREADS threads 
		int start = 0, end = 0;
		int diff = commitFiles.length / N_THREADS;		
		for(int thread = 0; thread < N_THREADS; thread++) {
			start = end;
			end = end + diff;			
			if(thread == (N_THREADS - 1))
				end = commitFiles.length;
			
			t[thread] = new Thread(new PairIssuesWithCommitsInRange(start, end, commitFiles));
			t[thread].start();
		}		
		
		//check if threads are still alive
		boolean go = false;
		while(go == false) {
			for(int i = 0; i < N_THREADS; i++) {
				if(t[i].isAlive()) {
					go = false;
					break;
				} else go = true;
			}
		}
		
		//after threads are finished
		pw_similaritiesFile.close();
		System.out.println("Processing logs and generating issue-commit links... [done]\n");
		
		if(trainingSetFilepath != null && !trainingSetFilepath.isEmpty()) {
			//Predict and separate positive traceability links
			TrainData.trainData(trainingSetFilepath, classifierFilepath, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON, f11_ON, f12_ON, f13_ON);
			Predict.predict(similaritiesFilepath, predictionFilepath, classifierFilepath);
			Predict.separatePositiveTraceabilityLinks(predictionFilepath, resultSetFilepath);
		}
	}	
	
	private void doThis(int start, int end, File[] commitFiles) throws JAXBException, FileNotFoundException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy hh:mm:ss a", Locale.ENGLISH);
		for(int index = start; index < end; index++) {
			
			JAXBContext jaxbCommit = JAXBContext.newInstance(Commit.class);
			Commit commit = (Commit) jaxbCommit.createUnmarshaller().unmarshal(new FileReader(commitFiles[index]));	
			String commitInfo = commit.getCommitMessage().trim();

			//Get and prepare date features for comparison
			Calendar commitDate = Calendar.getInstance();				
			commitDate.setTime(sdf.parse(commit.getCommitDate()));
			
			System.out.println("Linking issues to commit: " + commit.getCommitID() + " (" + (index + 1) + "/" + end + ")");
						
			File[] issueFiles = new File(workingDirectory + "issues/preprocessed/").listFiles();
			for(File iFile : issueFiles) {
	
				JAXBContext jaxbIssue = JAXBContext.newInstance(Issue.class);
				Issue issue = (Issue) jaxbIssue.createUnmarshaller().unmarshal(new FileReader(iFile));		
				String issueInfo = issue.getIssueDescription().trim();
				
				//Get and prepare date features for comparison
				Calendar issueReportDate = Calendar.getInstance();
				Calendar issueUpdateDate = Calendar.getInstance();
				issueReportDate.setTime(sdf.parse(issue.getIssueTimeCreated()));
				issueUpdateDate.setTime(sdf.parse(issue.getIssueTimeUpdated()));	
				
				//calculate features
				String features_result = featuresProcessor.calculateFeatures(issue.getIssueID(), commit.getCommitID(), 
					issueInfo, commitInfo, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON, f11_ON, f12_ON, f13_ON, commitDate, issueReportDate, issueUpdateDate);
				
				//output issue-commit link
				if(features_result != null) 
					pw_similaritiesFile.println(features_result);
			}
		}
	}
}
