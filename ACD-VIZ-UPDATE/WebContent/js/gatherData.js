document.getElementById("select-dataFolder-label-area").addEventListener("click", runTool);
document.getElementById("closeCollection").addEventListener("click" , hidePromptScreen );
//document.getElementById("submit").addEventListener("click" , hidePromptScreen );



function runTool() {
	showPromptScreen(); 	//show screen that takes data to process and run tool
}

function showPromptScreen() {
	document.getElementById("select-folder").value = null;
	document.getElementById("select-dataFolder-label-area").style.display = 'none';
	document.getElementById("select-folder-label-area").style.display = 'none';
	document.getElementById("data-collection").style.display = 'block';
}

function hidePromptScreen() { 
	document.getElementById("data-collection").style.display = 'none';
	document.getElementById("select-folder").value = null;
	document.getElementById("select-dataFolder-label-area").style.display = 'inline-block';
	document.getElementById("select-folder-label-area").style.display = 'inline-block';
	document.getElementById("data-collection").style.display = 'none';
}


function sendToolExeForm(e) {
	var ciLoc = documet.getElementById("commitIssueLocation").files;
	var cnLoc = document.getElementById("commitNewsLocation").files;
	
	
	if( (ciLoc.length === 0 && cnLoc.length !== 0) || (ciLoc.length !== 0 && cnLoc.length === 0)) {
		alert("Both Training Set Fields Must Contain .arff Files, if Training Set is to be Used!");
		e.preventDefault();
		return false;
	}
	
	var f1 = document.getElementById("numCmnTerms").checked;
	var f2 = document.getElementById("cosSim").checked;
	var f3 = document.getElementById("maxCos").checked;
	var f4 = document.getElementById("avgCos").checked;
	var f5 = document.getElementById("wCosFeat").checked;
	var f6 = document.getElementById("wCosArch").checked;
	var f7 = document.getElementById("wCosOper").checked;
	var f8 = document.getElementById("ngrmFeat").checked;
	var f9 = document.getElementById("ngrmArch").checked;
	var f10 = document.getElementById("ngrmOper").checked;
	var f11 = document.getElementById("issue-reportCommitIssueDate").checked;
	var f12 = document.getElementById("commitIssue-reportDate").checked;
	var f13 = document.getElementById("issue-updateCommiteDate").checked;
	
	if(!f1 && !f2 && !f3 && !f4 && !f5 && !f6 && !f7 && !f8 && !f9 && !f10 && !f11 && !f12 && !f13) {
		alert("At Least ONE Feature MUST be Selected!");
		e.preventDefault();
		return false;
	}
}


function checkboxSetF5F7(obj) {
	var val = document.getElementById(obj.id).checked;
	
	if(confirm("Features F5-F7 are Part of a Set and Must be Selected As One, Continue?") == true) {
		if(obj.id === "wCosFeat"){
			document.getElementById("wCosArch").checked = val;
			document.getElementById("wCosOper").checked = val;
		} else if(obj.id === "wCosArch"){
			document.getElementById("wCosFeat").checked = val;
			document.getElementById("wCosOper").checked = val;
		} else if(obj.id === "wCosOper"){
			document.getElementById("wCosArch").checked = val;
			document.getElementById("wCosFeat").checked = val;
		}  
	}
}

function checkboxSetF8F10(obj) {
	var val = document.getElementById(obj.id).checked;
	
	if(confirm("Features F8-F10 are Part of a Set and Must be Selected As One, Continue?") == true) {
		if(obj.id === "ngrmFeat"){
			document.getElementById("ngrmArch").checked = val;
			document.getElementById("ngrmOper").checked = val;
		} else if(obj.id === "ngrmArch"){
			document.getElementById("ngrmFeat").checked = val;
			document.getElementById("ngrmOper").checked = val;
		} else if(obj.id === "ngrmOper"){
			document.getElementById("ngrmFeat").checked = val;
			document.getElementById("ngrmArch").checked = val;
		}  
	}
}

