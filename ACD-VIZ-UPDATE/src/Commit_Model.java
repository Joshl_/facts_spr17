import java.io.Serializable;
import java.util.ArrayList;

public class Commit_Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String commitId;
	private String commitDate;
	private String commitAuthor;
	private String commitMessage;
	private String commitPackage;
	private String commitClass;
	private String commitLine;
	private ArrayList<News_Model> news = new ArrayList<>();;
	private ArrayList<Issue_Model> issues = new ArrayList<>();;
	
	public String getCommitId() {
		return commitId;
	}
	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}
	public String getCommitDate() {
		return commitDate;
	}
	public void setCommitDate(String commitDate) {
		this.commitDate = commitDate;
	}
	public String getCommitAuthor() {
		return commitAuthor;
	}
	public void setCommitAuthor(String commitAuthor) {
		this.commitAuthor = commitAuthor;
	}
	public String getCommitMessage() {
		return commitMessage;
	}
	public void setCommitMessage(String commitMessage) {
		this.commitMessage = commitMessage;
	}
	public String getCommitPackage() {
		return commitPackage;
	}
	public void setCommitPackage(String commitPackage) {
		this.commitPackage = commitPackage;
	}
	public String getCommitClass() {
		return commitClass;
	}
	public void setCommitClass(String commitClass) {
		this.commitClass = commitClass;
	}
	public String getCommitLine() {
		return commitLine;
	}
	public void setCommitLine(String commitLine) {
		this.commitLine = commitLine;
	}
	public ArrayList<News_Model> getNews() {
		return news;
	}
	public void setNews(ArrayList<News_Model> news) {
		this.news = news;
	}
	public ArrayList<Issue_Model> getIssues() {
		return issues;
	}
	public void setIssues(ArrayList<Issue_Model> issues) {
		this.issues = issues;
	}
	
}
