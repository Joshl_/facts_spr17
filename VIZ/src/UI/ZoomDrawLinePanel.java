package UI;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JComponent;

public class ZoomDrawLinePanel extends DrawLineJPanel {

	private static final long serialVersionUID = 1L;
	
	private double zoomFactor;
	private AffineTransform zoom;
	
	public ZoomDrawLinePanel(ArrayList<Pair> listOfPairs) {
		super(listOfPairs);
		zoomFactor = 1;
		
		this.addZoomListener();
		this.addZoomedMouseClickListener();
		//zoom = new AffineTransform();
	}
	
	public ZoomDrawLinePanel(ArrayList<Pair> listOfPairs, double zoomFactor) {
		super(listOfPairs);
		this.zoomFactor = zoomFactor;
		
		this.addZoomListener();
		this.addZoomedMouseClickListener();
	}

	public ZoomDrawLinePanel(LayoutManager arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ZoomDrawLinePanel(boolean arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ZoomDrawLinePanel(LayoutManager arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub	
		Graphics2D g2 = (Graphics2D) g;
		int w = this.getWidth() / 2;
		int h = this.getHeight() / 2;
		
		
		/*zoom = g2.getTransform();
		zoom.scale(zoomFactor, zoomFactor);
		if((int)zoomFactor != 1 ){
			zoom.translate(w, h);
		}
		g2.setTransform(zoom);*/
		
		/*g2.translate(w/2, h/2);
		g2.scale(zoomFactor, zoomFactor);
		g2.translate(-w/2, -h/2);*/
				
		//this.alterComponentSize(zoomFactor);
		

		super.paintComponent(g);	
		
	}
	

	/*@Override
	public Dimension getPreferredSize() {
		// TODO Auto-generated method stub
		int w = this.getWidth() / 2;
		int h = this.getHeight() / 2;
		return new Dimension((int)(w * zoomFactor), (int)(h * zoomFactor));
	}*/

	public double getZoomFactor() {
		return zoomFactor;
	}

	public void setZoomFactor(double zoomFactor) {
		this.zoomFactor = zoomFactor;
	}

	private void addZoomListener() {
		JComponent c = this;
		this.addMouseWheelListener(new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				// TODO Auto-generated method stub
				int rotate = e.getWheelRotation();
				if( ((zoomFactor > 0.5 && rotate < 0) || (zoomFactor < 1 && rotate > 0)) && c.getComponentCount() > 0) {
					zoomFactor += rotate * 0.1;
				}
				
			    int width = (int) (getWidth() * zoomFactor);
			    int height = (int) (getHeight() * zoomFactor);
			    setPreferredSize(new Dimension(width, height));
				c.revalidate();
				c.repaint();
				
				if(c.getParent() != null) {
					c.getParent().revalidate();
					c.getParent().repaint();
				}
			}
		});
	}
	
	private void addZoomedMouseClickListener() {
		JComponent c = this;
		this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				//System.out.println(e.getX() + ", " + e.getY() + "    " + zoom.getScaleX() + ", "+ zoom.getScaleY());
				
				/*if((int)zoomFactor != 1) {
					int x = (int) (e.getX() * zoom.getScaleX());
					int y = (int) (e.getY() * zoom.getScaleY());
					System.err.println(x + "," + y + "   " + c.getComponentAt(x, y));
//					c.getComponentAt(x, y);
				}*/
			}
		});
		
	}
}
