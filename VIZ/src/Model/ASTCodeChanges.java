package Model;

import com.github.gumtreediff.client.Run;
import com.github.gumtreediff.tree.ITree;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.FileTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import com.github.gumtreediff.actions.ActionGenerator;
import com.github.gumtreediff.actions.model.Action;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.matchers.Matchers;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;

import java.io.*;
import java.net.URL;

import java.nio.file.Files;
import java.util.Collection;
import java.util.Iterator;

public class ASTCodeChanges implements Serializable {


    private String[] input = new String[3];

    public ASTCodeChanges(String location, String file, String commitID) {
        Run.initGenerators();

        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = null;
        try {
            repository = builder.setGitDir(new File(location))
                    .readEnvironment().findGitDir().build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Git git = new Git(repository);
        Iterable<RevCommit> log = null;
        try {
            log = git.log().call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }

        RevWalk walk = new RevWalk(repository);
        try {
            walk.markStart(walk.parseCommit(repository.resolve(commitID)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Iterator<RevCommit> iterator = walk.iterator();
        RevCommit rev0 = iterator.next();
        RevCommit rev1 = iterator.next();

        String directory = location.replace(".git", "");
        String fileLocation = directory + file;
        fileLocation = fileLocation.replace('/', '\\');

        File original = new File(fileLocation);
        File temp = new File(fileLocation + ".temp");
        File temp0 = new File(directory + "/temp0.java");
        temp0.delete();
        File temp1 = new File(directory + "/temp1.java");
        temp1.delete();
        original.renameTo(temp);

        try {
            git.checkout().setStartPoint(rev0).addPath(file).setCreateBranch(false).call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }

        original = new File(fileLocation);
        original.renameTo(temp0);

        try {
            git.checkout().setStartPoint(rev1).addPath(file).setCreateBranch(false).call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }

        original = new File(fileLocation);
        original.renameTo(temp1);

        temp.renameTo(original);

        input[1] = temp0.getAbsolutePath();
        input[2] = temp1.getAbsolutePath();

    }

    public void ASTSwing(){
        input[0] = "swingdiff";
        Run.main(input);
    }

    public void ASTWeb(){
        input[0] = "webdiff";
        Run.main(input);
    }

}
